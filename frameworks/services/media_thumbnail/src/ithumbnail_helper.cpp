/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define MLOG_TAG "Thumbnail"

#include "ithumbnail_helper.h"

#include "ability_manager_client.h"
#include "hitrace_meter.h"
#include "media_column.h"
#include "medialibrary_errno.h"
#include "media_file_utils.h"
#include "media_log.h"
#include "rdb_helper.h"
#include "single_kvstore.h"
#include "thumbnail_const.h"

using namespace std;
using namespace OHOS::DistributedKv;
using namespace OHOS::NativeRdb;

namespace OHOS {
namespace Media {
void IThumbnailHelper::GetThumbnailInfo(ThumbRdbOpt &opts, ThumbnailData &outData)
{
    if (opts.store == nullptr) {
        return;
    }
    if (!opts.path.empty()) {
        outData.path = opts.path;
        outData.id = opts.row;
        return;
    }
    string filesTableName = opts.table;
    int errCode = E_ERR;
    if (!opts.networkId.empty()) {
        filesTableName = opts.store->ObtainDistributedTableName(opts.networkId, opts.table, errCode);
    }
    if (filesTableName.empty()) {
        return;
    }
    opts.table = filesTableName;
    int err;
    ThumbnailUtils::QueryThumbnailInfo(opts, outData, err);
    if (err != E_OK) {
        MEDIA_ERR_LOG("query fail [%{public}d]", err);
    }
    return;
}

void IThumbnailHelper::CreateLcd(AsyncTaskData* data)
{
    GenerateAsyncTaskData* taskData = static_cast<GenerateAsyncTaskData*>(data);
    DoCreateLcd(taskData->opts, taskData->thumbnailData);
}

void IThumbnailHelper::CreateThumbnail(AsyncTaskData* data)
{
    GenerateAsyncTaskData* taskData = static_cast<GenerateAsyncTaskData*>(data);
    DoCreateThumbnail(taskData->opts, taskData->thumbnailData);
}

void IThumbnailHelper::AddAsyncTask(MediaLibraryExecute executor, ThumbRdbOpt &opts, ThumbnailData &data, bool isFront)
{
    shared_ptr<MediaLibraryAsyncWorker> asyncWorker = MediaLibraryAsyncWorker::GetInstance();
    if (asyncWorker == nullptr) {
        MEDIA_DEBUG_LOG("IThumbnailHelper::AddAsyncTask asyncWorker is null");
        return;
    }
    GenerateAsyncTaskData* taskData = new (nothrow)GenerateAsyncTaskData();
    if (taskData == nullptr) {
        MEDIA_DEBUG_LOG("IThumbnailHelper::GenerateAsyncTaskData taskData is null");
        return;
    }
    taskData->opts = opts;
    taskData->thumbnailData = data;

    shared_ptr<MediaLibraryAsyncTask> generateAsyncTask = make_shared<MediaLibraryAsyncTask>(executor, taskData);
    asyncWorker->AddTask(generateAsyncTask, isFront);
}

ThumbnailWait::ThumbnailWait(bool release) : needRelease_(release)
{}

ThumbnailWait::~ThumbnailWait()
{
    if (needRelease_) {
        Notify();
    }
}

ThumbnailMap ThumbnailWait::thumbnailMap_;
std::shared_mutex ThumbnailWait::mutex_;

static bool WaitFor(const shared_ptr<SyncStatus>& thumbnailWait, int waitMs, unique_lock<mutex> &lck)
{
    bool ret = thumbnailWait->cond_.wait_for(lck, chrono::milliseconds(waitMs),
        [thumbnailWait]() { return thumbnailWait->isSyncComplete_; });
    if (!ret) {
        MEDIA_INFO_LOG("IThumbnailHelper::Wait wait for lock timeout");
    }
    return ret;
}

WaitStatus ThumbnailWait::InsertAndWait(const string &id, bool isLcd)
{
    id_ = id;

    if (isLcd) {
        id_ += THUMBNAIL_LCD_SUFFIX;
    } else {
        id_ += THUMBNAIL_THUMB_SUFFIX;
    }
    unique_lock<shared_mutex> writeLck(mutex_);
    auto iter = thumbnailMap_.find(id_);
    if (iter != thumbnailMap_.end()) {
        auto thumbnailWait = iter->second;
        unique_lock<mutex> lck(thumbnailWait->mtx_);
        writeLck.unlock();
        bool ret = WaitFor(thumbnailWait, WAIT_FOR_MS, lck);
        if (ret) {
            return WaitStatus::WAIT_SUCCESS;
        } else {
            return WaitStatus::TIMEOUT;
        }
    } else {
        shared_ptr<SyncStatus> thumbnailWait = make_shared<SyncStatus>();
        thumbnailMap_.insert(ThumbnailMap::value_type(id_, thumbnailWait));
        return WaitStatus::INSERT;
    }
}

void ThumbnailWait::CheckAndWait(const string &id, bool isLcd)
{
    id_ = id;

    if (isLcd) {
        id_ += THUMBNAIL_LCD_SUFFIX;
    } else {
        id_ += THUMBNAIL_THUMB_SUFFIX;
    }
    shared_lock<shared_mutex> readLck(mutex_);
    auto iter = thumbnailMap_.find(id_);
    if (iter != thumbnailMap_.end()) {
        auto thumbnailWait = iter->second;
        unique_lock<mutex> lck(thumbnailWait->mtx_);
        readLck.unlock();
        WaitFor(thumbnailWait, WAIT_FOR_MS, lck);
    }
}

void ThumbnailWait::Notify()
{
    unique_lock<shared_mutex> writeLck(mutex_);
    auto iter = thumbnailMap_.find(id_);
    if (iter != thumbnailMap_.end()) {
        auto thumbnailWait = iter->second;
        thumbnailMap_.erase(iter);
        {
            unique_lock<mutex> lck(thumbnailWait->mtx_);
            writeLck.unlock();
            thumbnailWait->isSyncComplete_ = true;
        }
        thumbnailWait->cond_.notify_all();
    }
}

bool IThumbnailHelper::DoCreateLcd(ThumbRdbOpt &opts, ThumbnailData &data)
{
    ThumbnailWait thumbnailWait(true);
    auto ret = thumbnailWait.InsertAndWait(data.id, true);
    if (ret == WaitStatus::WAIT_SUCCESS) {
        return true;
    }

    if (opts.table == AudioColumn::AUDIOS_TABLE) {
        MEDIA_ERR_LOG("Can not create lcd for audio");
        return false;
    }

    if (!ThumbnailUtils::LoadSourceImage(data, opts.screenSize, false)) {
        if (opts.path.empty()) {
            MEDIA_ERR_LOG("LoadSourceImage faild, %{private}s", data.path.c_str());
            return false;
        } else {
            opts.path = "";
            GetThumbnailInfo(opts, data);
            string fileName = ThumbnailUtils::GetThumbPath(data.path, THUMBNAIL_THUMB_SUFFIX);
            if (access(fileName.c_str(), F_OK) == 0) {
                return true;
            }
            if (!ThumbnailUtils::LoadSourceImage(data, opts.screenSize, false)) {
                return false;
            }
        }
    }

    if (!ThumbnailUtils::CompressImage(data.source, data.lcd)) {
        MEDIA_ERR_LOG("CompressImage faild");
        return false;
    }

    int err = ThumbnailUtils::SaveFile(data, ThumbnailType::LCD);
    if (err < 0) {
        MEDIA_ERR_LOG("SaveLcd faild %{public}d", err);
        return false;
    }

    data.lcd.clear();
    if (!ThumbnailUtils::UpdateLcdInfo(opts, data, err)) {
        MEDIA_INFO_LOG("UpdateLcdInfo faild err : %{public}d", err);
        return false;
    }

    return true;
}

bool IThumbnailHelper::GenThumbnail(ThumbRdbOpt &opts, ThumbnailData &data, const ThumbnailType type)
{
    Size size;
    switch (type) {
        case ThumbnailType::MICRO:
            size = { DEFAULT_MICRO_SIZE, DEFAULT_MICRO_SIZE };
            break;
        case ThumbnailType::THUMB:
            size = { DEFAULT_THUMBNAIL_SIZE, DEFAULT_THUMBNAIL_SIZE };
            break;
        default:
            MEDIA_ERR_LOG("invalid thumbnail type: %{public}d", type);
            return false;
    }

    if (!ThumbnailUtils::LoadSourceImage(data, size)) {
        if (opts.path.empty()) {
            MEDIA_ERR_LOG("LoadSourceImage faild, %{private}s", data.path.c_str());
            return false;
        } else {
            opts.path = "";
            GetThumbnailInfo(opts, data);
            string suffix = (type == ThumbnailType::MICRO) ? THUMBNAIL_MICRO_SUFFIX : THUMBNAIL_THUMB_SUFFIX;
            string fileName = ThumbnailUtils::GetThumbPath(data.path, suffix);
            if (access(fileName.c_str(), F_OK) == 0) {
                return true;
            }
            if (!ThumbnailUtils::LoadSourceImage(data, size)) {
                return false;
            }
        }
    }
    if ((type != ThumbnailType::MICRO) && !ThumbnailUtils::CompressImage(data.source, data.thumbnail)) {
        MEDIA_ERR_LOG("CompressImage faild");
        return false;
    }

    int err = ThumbnailUtils::SaveFile(data, type);
    if (err < 0) {
        MEDIA_ERR_LOG("SaveThumbnailData faild %{public}d", err);
        return false;
    }
    data.thumbnail.clear();
    return true;
}

bool IThumbnailHelper::DoCreateThumbnail(ThumbRdbOpt &opts, ThumbnailData &data)
{
    ThumbnailWait thumbnailWait(true);
    auto ret = thumbnailWait.InsertAndWait(data.id, false);
    if (ret == WaitStatus::WAIT_SUCCESS) {
        return true;
    }

    if (!GenThumbnail(opts, data, ThumbnailType::THUMB)) {
        return false;
    }
    return true;
}

bool IThumbnailHelper::DoThumbnailSync(ThumbRdbOpt &opts, ThumbnailData &outData)
{
    ThumbnailConnection *conn = new (nothrow) ThumbnailConnection;
    if (conn == nullptr) {
        return false;
    }
    sptr<AAFwk::IAbilityConnection> callback(conn);
    int ret = conn->GetRemoteDataShareHelper(opts, callback);
    if (ret != E_OK) {
        return false;
    }

    vector<string> devices = { opts.networkId };
    MediaLibrarySyncOpts syncOpts;
    syncOpts.rdbStore = opts.store;
    syncOpts.table = MEDIALIBRARY_TABLE;
    syncOpts.bundleName = BUNDLE_NAME;
    syncOpts.row = opts.row;
    if (MediaLibrarySyncOperation::SyncPullTable(syncOpts, devices)) {
        MEDIA_INFO_LOG("GetThumbnailPixelMap SyncPullTable FALSE");
        return false;
    }
    auto resultSet = ThumbnailUtils::QueryThumbnailInfo(opts, outData, ret);
    if ((resultSet == nullptr)) {
        MEDIA_ERR_LOG("QueryThumbnailInfo Faild [ %{public}d ]", ret);
        return false;
    }
    return true;
}

void ThumbnailConnection::OnAbilityConnectDone(
    const AppExecFwk::ElementName &element, const sptr<IRemoteObject> &remoteObject, int resultCode)
{
    if (remoteObject == nullptr) {
        MEDIA_ERR_LOG("OnAbilityConnectDone failed, remote is nullptr");
        return;
    }
    {
        unique_lock<mutex> lock(status_.mtx_);
        dataShareProxy_ = iface_cast<IDataShareThumb>(remoteObject);
    }
    status_.cond_.notify_all();
}

void ThumbnailConnection::OnAbilityDisconnectDone(const AppExecFwk::ElementName &element, int resultCode)
{
    MEDIA_DEBUG_LOG("called begin %{public}d", resultCode);
    unique_lock<mutex> lock(status_.mtx_);
    dataShareProxy_ = nullptr;
}

int32_t ThumbnailConnection::GetRemoteDataShareHelper(ThumbRdbOpt &opts, sptr<AAFwk::IAbilityConnection> &callback)
{
    if ((opts.context == nullptr)) {
        MEDIA_ERR_LOG("context nullptr");
        return E_ERR;
    }

    AAFwk::Want want;
    want.SetElementName(BUNDLE_NAME, "DataShareExtAbility");
    want.SetDeviceId(opts.networkId);
    auto err = AAFwk::AbilityManagerClient::GetInstance()->ConnectAbility(want, callback, opts.context->GetToken());
    if (err != E_OK) {
        MEDIA_ERR_LOG("ConnectAbility failed %{public}d", err);
        return err;
    }
    unique_lock<mutex> lock(status_.mtx_);
    if (status_.cond_.wait_for(lock, chrono::seconds(WAIT_FOR_SECOND),
        [this] { return dataShareProxy_ != nullptr; })) {
        MEDIA_DEBUG_LOG("All Wait connect success.");
    } else {
        MEDIA_ERR_LOG("All Wait connect timeout.");
        return E_THUMBNAIL_CONNECT_TIMEOUT;
    }

    Uri distriuteGenUri(opts.uri + "/" + DISTRIBUTE_THU_OPRN_CREATE);
    DataShare::DataShareValuesBucket valuesBucket;
    valuesBucket.Put(MEDIA_DATA_DB_URI, opts.uri);
    auto ret = dataShareProxy_->Insert(distriuteGenUri, valuesBucket);
    MEDIA_DEBUG_LOG("called end ret = %{public}d", ret);
    return ret;
}
} // namespace Media
} // namespace OHOS
